---------------------------------------------------------------------
-- This file is part of Freedroid
--
-- Freedroid is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- Freedroid is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Freedroid; see the file COPYING. If not, write to the
-- Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
-- MA 02111-1307 USA
----------------------------------------------------------------------

title_screen{
background = "credits.jpg",
song = "HellFortressTwo.ogg",
text = [[
            FreedroidRPG 0.16

ПРОГРАММИРОВАНИЕ:

        Боб
        Сэмюэль Дегранд
        Майк Фляйшманн
        Скотт Фурри
        Артур Юйе
        Янн Хорн
        Джозеф Кусия
        Грегори Лозе
        Майкл Мендельсон
        Майлз Маккамон
        Майкл Паркс
        Сэмюэль Питуазе
        v4hn
        Xenux

НАПОЛНЕНИЕ ИГРЫ:

        Эми Батлер
        Infrared
        Рэймонд Дженнингс
        Jonatas L. Nogueira
        Matthias Krüger
        Майлз Маккамон

ГРАФИКА:

        Infrared
        Бастиан Салмела

МУЗЫКА:

        Ник Хагман

ПЕРЕВОДЫ:

        чешский:
          Ян Тойнар
          Зденек

        немецкий:
          JM Franz
          Matthias Krüger
          Link Mario

        французский:
          Sebastian Adam
          Алерион
          Сэмюэль Дегранд
          Николя Фаржье
          Бенжамен Ледюк
          Дыон Кханг Нгуен
          Xenux

        бразильский португальский:
          Anelise D. J.
          Луис Фелипе Феррейра да Силва
          Jonatas L. Nogueira

        шведский:
          Александр Форсберг

РАЗНОЕ:

        Эйно Кескитало
        Дыон Кханг Нгуен



            А ТАКЖЕ В ПРЕДЫДУЩИХ ВЕРСИЯХ:

ПРОГРАММИРОВАНИЕ:

        Мария Грация Аластра
        Педро Арана
        Кэтэлин Бадя
        Пьер Бурдон
        Брайан Си
        Симон Кастельян
        jcoral
        Stefan Kangas
        Pavaluca Matei
        Майкл Мендельсон
        Esa-Matti Mourujarvi
        Ari Mustonen
        Nicolas Pepin-Perreault
        Quentin Pradet
        Johannes Prix
        Reinhard Prix
        Alexander Solovets
        Philippe Tetar

НАПОЛНЕНИЕ ИГРЫ:

        Stefan Huszics
        James
        Sebastian Offermann
        Kurtis Parramore
        Niklas Spille
        Starminn
        Karol Swietlicki
        rudi_s
        Thor
        JK Wood

МУЗЫКА:

        Nick "Nario" Hagman
        Arvid Picciani
        "The Beginning"
            by 4t thieves
        "Daybreak"
            by realsmokers
        "Bleostrada"
            by stud

РАЗНОЕ:

        MBROLA — преобразование текста в речь
        eSpeak — преобразование текста в речь
        Ryan 'simcop2387' Voots
        Andrew A. Gill
        Zombie Ryushu
        Ted Cipicchio
        Доктор
        Саймон Ньютон
        Клинт Херрон
        Hike Danakian
        Иэн Гриффитс
        Дэвид Кремер
        Armin Winterer
]]
}
